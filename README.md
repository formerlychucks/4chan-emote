# 4chan Emote

Script to bring back 4chan emotes from April Fool's 2022

Can be installed as a Userscript (Tampermonkey/Greasemonkey) or a Firefox extension

## Installing

### Userscript

Create a new script in Tampermonkey or Greasemonkey

Paste the contents of userscript.js and save

### Extension

Download and unzip the code. Navigate to about:debugging#/runtime/this-firefox and click "Load a Temporary Add-on"

Navigate to where you unzipped and select manifest.json

This will install it as a temporary addon, temporary addons get removed when Firefox is closed.

Mozilla doesn't allow you to install addons permanently unless they're signed, now.

## Using

Type the emote names between two colons and anyone else who has the addon can see them.

The emotes are case sensitive (only because REEeee and REEEEE are two different emotes)

For a list of valid ones, look at emote.js

## Things to do

- More efficient replacement algorithm?

- UI?

- Sign and publish the addon

