// ==UserScript==
// @name         4chan Emotes
// @version      0.1
// @description  Bring back April Fool's 2022 emotes
// @author       Anon Anonymous
// @match        *://boards.4channel.org/*
// @match        *://boards.4chan.org/*
// @icon         https://gitlab.com/anonanonymous/4chan-emote/-/raw/master/emotes/6f0d4e37_POGGERS.png
// @require      https://gitlab.com/anonanonymous/4chan-emote/-/raw/master/emote.js
// ==/UserScript==

(function() {
	'use strict';
	emoteReplace();
})();

